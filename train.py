# coding=utf-8
# Copyright 2018 The Google AI Language Team Authors and The HuggingFace Inc. team.
# Copyright (c) 2018, NVIDIA CORPORATION.  All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Fine-tuning the library models for language modeling on a text file (GPT, GPT-2, BERT, RoBERTa).
GPT and GPT-2 are fine-tuned using a causal language modeling (CLM) loss while BERT and RoBERTa are fine-tuned
using a masked language modeling (MLM) loss.
"""

import logging
import math
import os
import pickle
import glob
import json
import time
import gzip
from torch.nn.utils.rnn import pad_sequence
from dataclasses import dataclass, field
from typing import Optional
from tokenization import MecabBasicTokenizer
from tqdm import tqdm
import torch
from filelock import FileLock
import functools
import operator
import numpy
import fileinput
import linecache
import random

from torch.utils.data import Dataset

from transformers import (
    WEIGHTS_NAME,
    BertTokenizer,
    BertJapaneseTokenizer,
    CONFIG_MAPPING,
    MODEL_WITH_LM_HEAD_MAPPING,
    AutoConfig,
    AutoModelWithLMHead,
    AutoTokenizer,
    DataCollatorForLanguageModeling,
    HfArgumentParser,
    # LineByLineTextDataset,
    PreTrainedTokenizer,
    # TextDataset,
    Trainer,
    TrainingArguments,
    set_seed
)
import random

logger = logging.getLogger(__name__)

MODEL_CONFIG_CLASSES = list(MODEL_WITH_LM_HEAD_MAPPING.keys())
MODEL_TYPES = tuple(conf.model_type for conf in MODEL_CONFIG_CLASSES)

from pathlib import Path

# import data_loading_utils
# CHUNK_SIZE = 1000000

SPECIAL_TOKENS = ["<endoftext>", "<speaker1>", "<speaker2>"]
SPECIAL_TOKENS_2 = ["<endoftext>", "<speaker1>", "<speaker2>", "</speaker1>", "</speaker2>"]
SPECIAL_TOKENS_3 = ["<eos>", "<bos>", "<pad>", "<speaker1>", "<speaker2>", "</speaker1>", "</speaker2>", "<personality>", "</personality>"]
SPECIAL_TOKENS_SQUAD = ["<endoftext>", "<context>", "<answers>", "<answer>", "<question>",  "</context>", "</answers>", "</answer>", "</question>"]
ATTR_TO_SPECIAL_TOKEN = {'bos_token': '<endoftext>', 'eos_token': '<endoftext>', 'pad_token': '<endoftext>', 'additional_special_tokens': ['<speaker1>', '<speaker2>']}
ATTR_TO_SPECIAL_TOKEN_2 = {'bos_token': '<endoftext>', 'eos_token': '<endoftext>', 'pad_token': '<endoftext>', 'additional_special_tokens': ['<speaker1>', '<speaker2>', "</speaker1>", "</speaker2>"]}
ATTR_TO_SPECIAL_TOKEN_3 = {'bos_token': '<bos>', 'eos_token': '<eos>', 'pad_token': '<pad>', 'additional_special_tokens': ['<speaker1>', '<speaker2>', "</speaker1>", "</speaker2>", "<personality>", "</personality>"]}
ATTR_TO_SPECIAL_TOKEN_SQUAD = {'bos_token': '<endoftext>', 'eos_token': '<endoftext>', 'pad_token': '<endoftext>', 'additional_special_tokens': [ "<context>", "<answers>", "<answer>", "<question>",  "</context>", "</answers>", "</answer>", "</question>"]}



@dataclass
class ModelArguments:
    """
    Arguments pertaining to which model/config/tokenizer we are going to fine-tune, or train from scratch.
    """

    model_name_or_path: Optional[str] = field(
        default=None,
        metadata={
            "help": "The model checkpoint for weights initialization. Leave None if you want to train a model from scratch."
        },
    )
    model_type: Optional[str] = field(
        default=None,
        metadata={"help": "If training from scratch, pass a model type from the list: " + ", ".join(MODEL_TYPES)},
    )
    config_name: Optional[str] = field(
        default=None, metadata={"help": "Pretrained config name or path if not the same as model_name"}
    )
    tokenizer_name: Optional[str] = field(
        default=None, metadata={"help": "Pretrained tokenizer name or path if not the same as model_name"}
    )
    cache_dir: Optional[str] = field(
        default=None, metadata={"help": "Where do you want to store the pretrained models downloaded from s3"}
    )

    bertJapaneseTokenizer: bool = field(
        default=False, metadata={"help": "Whether to run bert Japanese tokenizer."}
    )
    bertTokenizer: bool = field(
        default=False, metadata={"help": "Whether to run bert tokenizer."}
    )
    bpeTokenizer: bool = field(
        default=False, metadata={"help": "Whether to run the default bpe  tokenizer."}
    )  # hugging face library
    bpeChar: bool = field(
        default=False, metadata={"help": "Whether to run bpe char tokenizer."}
    )  # hugging face library
    bpeSentence: bool = field(
        default=False, metadata={"help": "Whether to run  bpe sentence tokenizer."}
    )  # hugging face library
    sentencePiece: bool = field(
        default=False, metadata={"help": "Whether to use sentence piece to tokenize the data."}
    )  # hugging face library
    sentencePieceChar: bool = field(
        default=False, metadata={"help": "Whether to use char sentence piece to tokenize the data."}
    )  # sentence piece library
    sentencePieceWord: bool = field(
        default=False, metadata={"help": "Whether to use word sentence piece to tokenize the data."}
    )  # sentence piece library
    sentencePieceBpe: bool = field(
        default=False, metadata={"help": "Whether to use bpe sentence piece to tokenize the data."}
    )  # sentence piece library

    wordPiece: bool = field(
        default=False, metadata={"help": "Whether to use bert word piece to tokenize the data."}
    )
    mecab_dict_path: str = field(
        default=None, metadata={"help": "Mecab dictionary for tokenization."}
    )
    useMecab: bool = field(
        default=False, metadata={"help": "Whether to tokenize the input."}
    )

    eval_all_checkpoints: bool = field(
        default=False,
        metadata={
            "help": "Evaluate all checkpoints starting with the same prefix as model_name_or_path ending and ending with step number."}
    )


@dataclass
class DataTrainingArguments:
    """
    Arguments pertaining to what data we are going to input our model for training and eval.
    """

    train_data_file: Optional[str] = field(
        default=None, metadata={"help": "The input training data file (a text file)."}
    )
    eval_data_file: Optional[str] = field(
        default=None,
        metadata={"help": "An optional input evaluation data file to evaluate the perplexity on (a text file)."},
    )
    line_by_line: bool = field(
        default=False,
        metadata={"help": "Whether distinct lines of text in the dataset are to be handled as distinct sequences."},
    )
    personaTextDataset: bool = field(
        default=False,
        metadata={"help": "Whether distinct lines of text in the dataset are to be handled as distinct sequences."},
    )
    SQUADDataset: bool = field(
        default=False,
        metadata={"help": "Whether distinct lines of text in the dataset are to be handled as distinct sequences."},
    )
    mlm: bool = field(
        default=False, metadata={"help": "Train with masked-language modeling loss instead of language modeling."}
    )
    mlm_probability: float = field(
        default=0.15, metadata={"help": "Ratio of tokens to mask for masked language modeling loss"}
    )

    block_size: int = field(
        default=-1,
        metadata={
            "help": "Optional input sequence length after tokenization."
                    "The training dataset will be truncated in block of this size for training."
                    "Default to the model max input length for single sentence inputs (take into account special tokens)."
        },
    )
    overwrite_cache: bool = field(
        default=False, metadata={"help": "Overwrite the cached training and evaluation sets"}
    )


def add_special_tokens_(model, tokenizer, ATTR_TO_SPECIAL_TOKEN_):
    """ Add special tokens to the tokenizer and the model if they have not already been added. """
    orig_num_tokens = len(tokenizer)
    num_added_tokens = tokenizer.add_special_tokens(ATTR_TO_SPECIAL_TOKEN_) # doesn't add if they are already there
    if num_added_tokens > 0:
        model.resize_token_embeddings(new_num_tokens=orig_num_tokens + num_added_tokens)

def flatten(list_):
    return functools.reduce(operator.iconcat, list_, [])

class TextDataset(Dataset):
    """TextDatasetAozora
        * Add [CLS] into the beginning of a sentence
        * Add [SEP] into the end of a sentence
        """
    def __init__(self,  model, tokenizer: PreTrainedTokenizer, args, file_path: str, block_size=1024, overwrite_cache=False):

        assert os.path.isfile(file_path)
        tokenizer_name = None
        if args.model_name_or_path is not None:  # if continuing from checkpoint or finetuning
            vocab_txt = os.path.join(args.model_name_or_path, "vocab.txt")
            vocab_json = os.path.join(args.model_name_or_path, "vocab.json")
            tokenizer_name = args.model_name_or_path
        else:
            vocab_txt = os.path.join(args.tokenizer_name, "vocab.txt")
            vocab_json = os.path.join(args.tokenizer_name, "vocab.json")
            tokenizer_name = args.tokenizer_name

        directory, filename = os.path.split(file_path)
        cached_features_file = os.path.join(
            directory,
            args.model_type + "_text_dataset_cached_lm_" + str(block_size) + "_" + filename + ".pkl"
        )
        add_special_tokens_(model, tokenizer, ATTR_TO_SPECIAL_TOKEN_2)

        if os.path.exists(cached_features_file) and not overwrite_cache:
            logger.info("Loading features from cached file %s", cached_features_file)
            with open(cached_features_file, "rb") as handle:
                self.examples = pickle.load(handle)
        else:
            logger.info("Creating features from dataset file at %s", directory)
            is_gzip = True if file_path[-3:] == '.gz' else False

            self.examples = []

            f = gzip.open(file_path, 'rt', encoding='utf8') if is_gzip else open(file_path, 'r', encoding='utf8')
            documents = flatten([doc.split('DIALOGUE END') for doc in f.read().split('DIALOGUE_END')])
            logger.info("Read {} documents".format(len(documents)))

            # selecting tokenizer
            if args.sentencePiece:  # sentence piece tokenizer
                if args.sentencePieceChar:  # char sentence piece
                    if os.path.exists(vocab_txt):
                        logger.info(f"Loading char sentence piece tokenizer")
                        from tokenizers import BertWordPieceTokenizer
                        tokenizer = BertWordPieceTokenizer(os.path.join(tokenizer_name, "vocab.txt"),
                                                           handle_chinese_chars=True, lowercase=False)
                        tokenizer.enable_truncation(block_size)

                elif args.sentencePieceWord:  # word sentence piece
                    if os.path.exists(vocab_txt):
                        logger.info(f"Loading word sentence piece tokenizer")
                        from tokenizers import BertWordPieceTokenizer
                        tokenizer = BertWordPieceTokenizer(os.path.join(tokenizer_name, "vocab.txt"),
                                                           handle_chinese_chars=True, lowercase=False)
                        tokenizer.enable_truncation(block_size)
                elif args.sentencePieceBpe:  # bpe sentence piece
                    if os.path.exists(vocab_txt):
                        logger.info(f"Loading bpe sentence piece tokenizer")
                        from tokenizers import BertWordPieceTokenizer
                        tokenizer = BertWordPieceTokenizer(os.path.join(tokenizer_name, "vocab.txt"),
                                                           handle_chinese_chars=True, lowercase=False)
                        tokenizer.enable_truncation(block_size)
                else:  # sentence piece
                    if os.path.exists(vocab_json): logger.info(f"Loading sentence piece tokenizer")
                    from tokenizers import SentencePieceBPETokenizer
                    tokenizer = SentencePieceBPETokenizer(
                        os.path.join(tokenizer_name, "vocab.json"),
                        os.path.join(tokenizer_name, "merges.txt"))
                    tokenizer.enable_truncation(block_size)

            elif args.wordPiece and os.path.exists(vocab_txt):  # word peice tokenizer
                bert_tokenizer = os.path.join(tokenizer_name, "vocab.txt")
                if os.path.exists(bert_tokenizer):
                    logger.info("Loading BERT word piece tokenizer")
                    from tokenizers import BertWordPieceTokenizer
                    tokenizer = BertWordPieceTokenizer(os.path.join(tokenizer_name, "vocab.txt"),
                                                       handle_chinese_chars=True, lowercase=False)
                    tokenizer.enable_truncation(block_size)

            elif args.bpeTokenizer:  # bpe tokenizer
                if args.bpeChar:  # bpe char
                    if os.path.exists(vocab_json):
                        logger.info(f"Loading char BPE tokenizer")
                        from tokenizers import CharBPETokenizer
                        tokenizer = CharBPETokenizer(
                            os.path.join(tokenizer_name, "vocab.json"),
                            os.path.join(tokenizer_name, "merges.txt")
                        )
                        tokenizer.enable_truncation(max_length=block_size)

                elif args.bpeSentence:  # bpe sentence
                    if os.path.exists(vocab_json):
                        logger.info(f"Loading sentence BPE tokenizer")
                        from tokenizers import SentencePieceBPETokenizer
                        tokenizer = SentencePieceBPETokenizer(
                            os.path.join(tokenizer_name, "vocab.json"),
                            os.path.join(tokenizer_name, "merges.txt")
                        )
                        tokenizer.enable_truncation(max_length=block_size)
                else:  # bpe
                    logger.info("Loading BPE tokenizer")

            logger.info("Reading file %s", file_path)

            # add_special_tokens_(model, tokenizer, ATTR_TO_SPECIAL_TOKEN)
            # eos, speaker1, speaker2, = tokenizer.convert_tokens_to_ids(SPECIAL_TOKENS)

            eos, speaker1, speaker2, speaker1_end, speaker2_end = tokenizer.convert_tokens_to_ids(SPECIAL_TOKENS_2)

            if args.useMecab:  # if tokenized input(use mecab tokenization before using our trained tokenizer)
                mecab_tokenizer = MecabBasicTokenizer(do_lower_case=True,
                                                      mecab_dict_path=args.mecab_dict_path)
            for text in tqdm(documents):
                tokenized_text = []
                for i, sentence in enumerate(text.split('\n')):
                    if len(sentence) > 1 and sentence != "\n" and "THEME" not in sentence and 'DIALOGUE_END' not in sentence:
                        speaker = speaker1 if "A" in sentence[:3] else speaker2
                        speaker_end = speaker1_end if i % 2 == 0 else speaker2_end
                        sentence = sentence[3:]
                        if args.useMecab:  # if tokenized input(use mecab tokenization before using our trained tokenizer)
                            tokenized_sentence = mecab_tokenizer.tokenize(sentence)
                            tokenized_sentence = tokenizer.encode(" ".join(tokenized_sentence))
                            # if isConversationOver:
                            ids = [speaker] + tokenized_sentence + [speaker_end]
                            tokenized_text.extend(ids)
                        else:
                            tokenized_sentence = tokenizer.encode(sentence)
                            tokenized_text.extend(tokenized_sentence)
                tokenized_text = [eos] + tokenized_text + [eos]
                if len(tokenized_text) <= block_size:
                    self.examples.append(
                        tokenizer.build_inputs_with_special_tokens(tokenized_text)
                    )
                else:
                    for i in range(0, len(tokenized_text) - block_size + 1, block_size):
                        self.examples.append(
                            tokenizer.build_inputs_with_special_tokens(tokenized_text[i: i + block_size])
                        )


            logger.info("Saving features into cached file %s", cached_features_file)
            with open(cached_features_file, "wb") as handle:
                pickle.dump(self.examples, handle, protocol=pickle.HIGHEST_PROTOCOL)

    def __len__(self):
        return len(self.examples)

    def __getitem__(self, item):
        return torch.tensor(self.examples[item])


class SQUADDataset(Dataset):
    """TextDatasetAozora
        * Add [CLS] into the beginning of a sentence
        * Add [SEP] into the end of a sentence
        """
    def __init__(self,  model, tokenizer: PreTrainedTokenizer, args, file_path: str, block_size=1024, overwrite_cache=False):

        assert os.path.isfile(file_path)
        tokenizer_name = None
        if args.model_name_or_path is not None:  # if continuing from checkpoint or finetuning
            vocab_txt = os.path.join(args.model_name_or_path, "vocab.txt")
            vocab_json = os.path.join(args.model_name_or_path, "vocab.json")
            tokenizer_name = args.model_name_or_path
        else:
            vocab_txt = os.path.join(args.tokenizer_name, "vocab.txt")
            vocab_json = os.path.join(args.tokenizer_name, "vocab.json")
            tokenizer_name = args.tokenizer_name

        directory, filename = os.path.split(file_path)
        cached_features_file = os.path.join(
            directory,
            args.model_type + "_text_dataset_cached_lm_" + str(block_size) + "_" + filename + ".pkl"
        )
        add_special_tokens_(model, tokenizer, ATTR_TO_SPECIAL_TOKEN_SQUAD)

        if os.path.exists(cached_features_file) and not overwrite_cache:
            logger.info("Loading features from cached file %s", cached_features_file)
            with open(cached_features_file, "rb") as handle:
                self.examples = pickle.load(handle)
        else:
            logger.info("Creating features from dataset file at %s", directory)
            is_gzip = True if file_path[-3:] == '.gz' else False

            self.examples = []

            f = gzip.open(file_path, 'rt', encoding='utf8') if is_gzip else open(file_path, 'r', encoding='utf8')
            documents = f.read().split('DIALOGUE END')
            logger.info("Read {} documents".format(len(documents)))

            logger.info("Reading file %s", file_path)

            # add_special_tokens_(model, tokenizer, ATTR_TO_SPECIAL_TOKEN)
            eos, context, answers, answer, question, context_end, answers_end, answer_end, question_end = tokenizer.convert_tokens_to_ids(SPECIAL_TOKENS_SQUAD)

            if args.useMecab:  # if tokenized input(use mecab tokenization before using our trained tokenizer)
                mecab_tokenizer = MecabBasicTokenizer(do_lower_case=True,
                                                      mecab_dict_path=args.mecab_dict_path)
            no_invalid_cnt = 0

            for text in tqdm(documents):
                question_ = None
                answer_ = None
                context_ = None
                for i, sentence in enumerate(text.split('\n')):
                    if len(sentence) > 1 and sentence != "\n":
                        if sentence == 'CONTEXT: ' or sentence == 'QUESTION: ' or sentence == 'ANSWER: ' or sentence == 'CONTEXT: ':
                            no_invalid_cnt +=1
                            break
                        if 'QUESTION' in sentence:
                            tokenized_sentence = mecab_tokenizer.tokenize(sentence[10:])
                            tokenized_sentence = tokenizer.encode(" ".join(tokenized_sentence))
                            question_ = [question] + tokenized_sentence + [question_end]
                        elif 'ANSWER' in sentence:
                            tokenized_sentence = mecab_tokenizer.tokenize(sentence[5:])
                            tokenized_sentence = tokenizer.encode(" ".join(tokenized_sentence))
                            answer_ = [answer] + tokenized_sentence + [answer_end]
                        elif 'CONTEXT' in sentence:
                            tokenized_sentence = mecab_tokenizer.tokenize(sentence[9:])
                            tokenized_sentence = tokenizer.encode(" ".join(tokenized_sentence))
                            context_ = [context] + tokenized_sentence + [context_end]
                        elif 'TITLE' in sentence:
                            pass

                if question_ is not None and answer_ is not None and context_ is not None:
                    tokenized_text = [eos] + context_ + question_ + answer_ + [eos]
                    if len(tokenized_text) <= block_size:
                        self.examples.append(
                            tokenizer.build_inputs_with_special_tokens(tokenized_text)
                        )
                    else:
                        for i in range(0, len(tokenized_text) - block_size + 1, block_size):
                            self.examples.append(
                                tokenizer.build_inputs_with_special_tokens(tokenized_text[i: i + block_size])
                            )
                else:
                    no_invalid_cnt +=1


            logger.info("No of invalid examples are: %s", no_invalid_cnt)
            logger.info("Saving features into cached file %s", cached_features_file)
            with open(cached_features_file, "wb") as handle:
                pickle.dump(self.examples, handle, protocol=pickle.HIGHEST_PROTOCOL)

    def __len__(self):
        return len(self.examples)

    def __getitem__(self, item):
        return torch.tensor(self.examples[item])


class PersonaTextDataset(Dataset):
    """TextDatasetAozora
        * Add [CLS] into the beginning of a sentence
        * Add [SEP] into the end of a sentence
        """
    def __init__(self,  model, tokenizer: PreTrainedTokenizer, args, file_path: str, block_size=1024, overwrite_cache=False):

        assert os.path.isfile(file_path)
        tokenizer_name = None
        if args.model_name_or_path is not None:  # if continuing from checkpoint or finetuning
            vocab_txt = os.path.join(args.model_name_or_path, "vocab.txt")
            vocab_json = os.path.join(args.model_name_or_path, "vocab.json")
            tokenizer_name = args.model_name_or_path
        else:
            vocab_txt = os.path.join(args.tokenizer_name, "vocab.txt")
            vocab_json = os.path.join(args.tokenizer_name, "vocab.json")
            tokenizer_name = args.tokenizer_name

        directory, filename = os.path.split(file_path)
        cached_features_file = os.path.join(
            directory,
            args.model_type + "_text_dataset_cached_lm_" + str(block_size) + "_" + filename + ".pkl"
        )
        add_special_tokens_(model, tokenizer, ATTR_TO_SPECIAL_TOKEN_3)

        if os.path.exists(cached_features_file) and not overwrite_cache:
            logger.info("Loading features from cached file %s", cached_features_file)
            with open(cached_features_file, "rb") as handle:
                self.examples = pickle.load(handle)
        else:
            logger.info("Creating features from dataset file at %s", directory)
            is_gzip = True if file_path[-3:] == '.gz' else False

            self.examples = []

            f = gzip.open(file_path, 'rt', encoding='utf8') if is_gzip else open(file_path, 'r', encoding='utf8')
            documents = flatten([doc.split('DIALOGUE END') for doc in f.read().split('DIALOGUE_END')])
            logger.info("Read {} documents".format(len(documents)))

            # selecting tokenizer
            if args.sentencePiece:  # sentence piece tokenizer
                if args.sentencePieceChar:  # char sentence piece
                    if os.path.exists(vocab_txt):
                        logger.info(f"Loading char sentence piece tokenizer")
                        from tokenizers import BertWordPieceTokenizer
                        tokenizer = BertWordPieceTokenizer(os.path.join(tokenizer_name, "vocab.txt"),
                                                           handle_chinese_chars=True, lowercase=False)
                        tokenizer.enable_truncation(block_size)

                elif args.sentencePieceWord:  # word sentence piece
                    if os.path.exists(vocab_txt):
                        logger.info(f"Loading word sentence piece tokenizer")
                        from tokenizers import BertWordPieceTokenizer
                        tokenizer = BertWordPieceTokenizer(os.path.join(tokenizer_name, "vocab.txt"),
                                                           handle_chinese_chars=True, lowercase=False)
                        tokenizer.enable_truncation(block_size)
                elif args.sentencePieceBpe:  # bpe sentence piece
                    if os.path.exists(vocab_txt):
                        logger.info(f"Loading bpe sentence piece tokenizer")
                        from tokenizers import BertWordPieceTokenizer
                        tokenizer = BertWordPieceTokenizer(os.path.join(tokenizer_name, "vocab.txt"),
                                                           handle_chinese_chars=True, lowercase=False)
                        tokenizer.enable_truncation(block_size)
                else:  # sentence piece
                    if os.path.exists(vocab_json): logger.info(f"Loading sentence piece tokenizer")
                    from tokenizers import SentencePieceBPETokenizer
                    tokenizer = SentencePieceBPETokenizer(
                        os.path.join(tokenizer_name, "vocab.json"),
                        os.path.join(tokenizer_name, "merges.txt"))
                    tokenizer.enable_truncation(block_size)

            elif args.wordPiece and os.path.exists(vocab_txt):  # word peice tokenizer
                bert_tokenizer = os.path.join(tokenizer_name, "vocab.txt")
                if os.path.exists(bert_tokenizer):
                    logger.info("Loading BERT word piece tokenizer")
                    from tokenizers import BertWordPieceTokenizer
                    tokenizer = BertWordPieceTokenizer(os.path.join(tokenizer_name, "vocab.txt"),
                                                       handle_chinese_chars=True, lowercase=False)
                    tokenizer.enable_truncation(block_size)

            elif args.bpeTokenizer:  # bpe tokenizer
                if args.bpeChar:  # bpe char
                    if os.path.exists(vocab_json):
                        logger.info(f"Loading char BPE tokenizer")
                        from tokenizers import CharBPETokenizer
                        tokenizer = CharBPETokenizer(
                            os.path.join(tokenizer_name, "vocab.json"),
                            os.path.join(tokenizer_name, "merges.txt")
                        )
                        tokenizer.enable_truncation(max_length=block_size)

                elif args.bpeSentence:  # bpe sentence
                    if os.path.exists(vocab_json):
                        logger.info(f"Loading sentence BPE tokenizer")
                        from tokenizers import SentencePieceBPETokenizer
                        tokenizer = SentencePieceBPETokenizer(
                            os.path.join(tokenizer_name, "vocab.json"),
                            os.path.join(tokenizer_name, "merges.txt")
                        )
                        tokenizer.enable_truncation(max_length=block_size)
                else:  # bpe
                    logger.info("Loading BPE tokenizer")

            logger.info("Reading file %s", file_path)

            # add_special_tokens_(model, tokenizer, ATTR_TO_SPECIAL_TOKEN)
            # eos, speaker1, speaker2, = tokenizer.convert_tokens_to_ids(SPECIAL_TOKENS)
            if True:

                eos, speaker1, speaker2, speaker1_end, speaker2_end, persona, persona_end = tokenizer.convert_tokens_to_ids(SPECIAL_TOKENS_3)

                if args.useMecab:  # if tokenized input(use mecab tokenization before using our trained tokenizer)
                    mecab_tokenizer = MecabBasicTokenizer(do_lower_case=True,
                                                          mecab_dict_path=args.mecab_dict_path)
                for text in tqdm(documents):
                    tokenized_text = []
                    tokenized_persona = []
                    for i, sentence in enumerate(text.split('\n')):
                        if len(sentence) > 1 and sentence != "\n" and "THEME" not in sentence and 'DIALOGUE END' not in sentence:
                            if 'A' not in sentence[:3] and 'B' not in sentence[:3]:
                                tokenized_sentence = mecab_tokenizer.tokenize(sentence)
                                tokenized_sentence = tokenizer.encode(" ".join(tokenized_sentence))
                                tokenized_persona = [persona] + tokenized_sentence + [persona_end]
                            else:
                                speaker = speaker1 if "A" in sentence[:3] else speaker2
                                speaker_end = speaker1_end if i % 2 == 0 else speaker2_end
                                sentence = sentence[3:]
                                if args.useMecab:  # if tokenized input(use mecab tokenization before using our trained tokenizer)
                                    tokenized_sentence = mecab_tokenizer.tokenize(sentence)
                                    tokenized_sentence = tokenizer.encode(" ".join(tokenized_sentence))
                                    # if isConversationOver:
                                    ids = [speaker] + tokenized_sentence + [speaker_end]
                                    tokenized_text.extend(ids)
                                else:
                                    tokenized_sentence = tokenizer.encode(sentence)
                                    tokenized_text.extend(tokenized_sentence)
                    tokenized_text = [eos] + tokenized_persona + tokenized_text + [eos]
                    if len(tokenized_text) <= block_size:
                        self.examples.append(
                            tokenizer.build_inputs_with_special_tokens(tokenized_text)
                        )
                    else:
                        for i in range(0, len(tokenized_text) - block_size + 1, block_size):
                            self.examples.append(
                                tokenizer.build_inputs_with_special_tokens(tokenized_text[i: i + block_size])
                            )
            else:
                eos, bos, pad, speaker1, speaker2, speaker1_end, speaker2_end, persona, persona_end = tokenizer.convert_tokens_to_ids(
                    SPECIAL_TOKENS_3)

                if args.useMecab:  # if tokenized input(use mecab tokenization before using our trained tokenizer)
                    mecab_tokenizer = MecabBasicTokenizer(do_lower_case=True,
                                                          mecab_dict_path=args.mecab_dict_path)
                permutation = 3
                for text in tqdm(documents):
                    tokenized_text = []
                    tokenized_persona = []
                    for j in range(permutation):
                        tokenized_text_ = []
                        tokenized_persona_ = []
                        for i, sentence in enumerate(text.split('\n')):
                            if len(
                                    sentence) > 1 and sentence != "\n" and "THEME" not in sentence and 'DIALOGUE END' not in sentence:
                                if 'A' not in sentence[:3] and 'B' not in sentence[:3]:
                                    personas = sentence.split("。")
                                    random.shuffle(personas)
                                    for p in personas:
                                        if len(p) != 0:
                                            tokenized_sentence = mecab_tokenizer.tokenize(p + "。")
                                            tokenized_sentence = tokenizer.encode(" ".join(tokenized_sentence))
                                            tokenized_persona_.extend([persona] + tokenized_sentence + [persona_end])
                                else:
                                    speaker = speaker1 if "A" in sentence[:3] else speaker2
                                    speaker_end = speaker1_end if i % 2 == 0 else speaker2_end
                                    sentence = sentence[3:]
                                    if args.useMecab:  # if tokenized input(use mecab tokenization before using our trained tokenizer)
                                        tokenized_sentence = mecab_tokenizer.tokenize(sentence)
                                        tokenized_sentence = tokenizer.encode(" ".join(tokenized_sentence))
                                        ids = [speaker] + tokenized_sentence + [speaker_end]
                                        tokenized_text_.extend(ids)
                                    else:
                                        tokenized_sentence = tokenizer.encode(sentence)
                                        tokenized_text_.extend(tokenized_sentence)
                        tokenized_text.append(tokenized_text_)
                        tokenized_persona.append(tokenized_persona_)
                    for k in range(permutation):
                        tokenized_text_ = [bos] + tokenized_persona[k] + tokenized_text[k] + [eos]
                        tokenized_text_ = tokenized_text_ + padToMaxLength(len(tokenized_text_), pad, block_size)
                        if len(tokenized_text_) <= block_size:
                            self.examples.append(
                                tokenizer.build_inputs_with_special_tokens(tokenized_text_)
                            )
                        else:
                            for i in range(0, len(tokenized_text_) - block_size + 1, block_size):
                                self.examples.append(
                                    tokenizer.build_inputs_with_special_tokens(tokenized_text_[i: i + block_size])
                                )

            logger.info("Saving features into cached file %s", cached_features_file)
            with open(cached_features_file, "wb") as handle:
                pickle.dump(self.examples, handle, protocol=pickle.HIGHEST_PROTOCOL)

    def __len__(self):
        return len(self.examples)

    def __getitem__(self, item):
        return torch.tensor(self.examples[item])


class LineByLineTextDataset(Dataset):
    def __init__(self, model, tokenizer: PreTrainedTokenizer, args, file_path: str, block_size=1024, overwrite_cache=False):
        assert os.path.isfile(file_path)
        logger.info("Creating features from dataset file at %s", file_path)
        tokenizer_name = None
        directory, filename = os.path.split(file_path)
        cached_features_file = os.path.join(
            directory,
            args.model_type + "_line_by_line_dataset_cached_lm_" + str(block_size) + "_" + filename
        )

        # Make sure only the first process in distributed training processes the dataset,
        # and the others will use the cache.
        lock_path = cached_features_file + ".lock"
        add_special_tokens_(model, tokenizer, ATTR_TO_SPECIAL_TOKEN_2)
        with FileLock(lock_path):
            if os.path.exists(cached_features_file) and not overwrite_cache:
                start = time.time()

                logger.info("Loading features from cached file %s", cached_features_file)
                # with open(cached_features_file, "r") as handle:
                #     self.examples = eval(json.load(handle)['examples'])
                #     print(self.examples)
                self.examples = torch.load(cached_features_file)
                # self.examples = json.loads(cached_features_file)
                logger.info(
                    f"Loaded features from cached file {cached_features_file} [took %.3f s]", time.time() - start
                )
            else:
                if args.model_name_or_path is not None:  # if continuing from checkpoint or finetuning
                    vocab_txt = os.path.join(args.model_name_or_path, "vocab.txt")
                    vocab_json = os.path.join(args.model_name_or_path, "vocab.json")
                    tokenizer_name = args.model_name_or_path
                else:
                    vocab_txt = os.path.join(args.tokenizer_name, "vocab.txt")
                    vocab_json = os.path.join(args.tokenizer_name, "vocab.json")
                    tokenizer_name = args.tokenizer_name

                if args.sentencePiece:  # sentence piece tokenizer
                    if args.sentencePieceChar:  # char sentence piece
                        if os.path.exists(vocab_txt):
                            logger.info(f"Loading char sentence piece tokenizer")
                            from tokenizers import BertWordPieceTokenizer
                            tokenizer = BertWordPieceTokenizer(os.path.join(tokenizer_name, "vocab.txt"),
                                                               handle_chinese_chars=True, lowercase=False)
                            tokenizer.enable_truncation(block_size)

                    elif args.sentencePieceWord:  # word sentence piece
                        if os.path.exists(vocab_txt):
                            logger.info(f"Loading word sentence piece tokenizer")
                            from tokenizers import BertWordPieceTokenizer
                            tokenizer = BertWordPieceTokenizer(os.path.join(tokenizer_name, "vocab.txt"),
                                                               handle_chinese_chars=True, lowercase=False)
                            tokenizer.enable_truncation(block_size)
                    elif args.sentencePieceBpe:  # bpe sentence piece
                        if os.path.exists(vocab_txt):
                            logger.info(f"Loading bpe sentence piece tokenizer")
                            from tokenizers import BertWordPieceTokenizer
                            tokenizer = BertWordPieceTokenizer(os.path.join(tokenizer_name, "vocab.txt"),
                                                               handle_chinese_chars=True, lowercase=False)
                            tokenizer.enable_truncation(block_size)
                    else:  # sentence piece
                        if os.path.exists(vocab_json): logger.info(f"Loading sentence piece tokenizer")
                        from tokenizers import SentencePieceBPETokenizer
                        tokenizer = SentencePieceBPETokenizer(
                            os.path.join(tokenizer_name, "vocab.json"),
                            os.path.join(tokenizer_name, "merges.txt"))
                        tokenizer.enable_truncation(block_size)

                elif args.wordPiece and os.path.exists(vocab_txt):  # word peice tokenizer
                    bert_tokenizer = os.path.join(tokenizer_name, "vocab.txt")
                    if os.path.exists(bert_tokenizer):
                        logger.info("Loading BERT word piece tokenizer")
                        from tokenizers import BertWordPieceTokenizer
                        tokenizer = BertWordPieceTokenizer(os.path.join(tokenizer_name, "vocab.txt"),
                                                           handle_chinese_chars=True, lowercase=False)
                        tokenizer.enable_truncation(block_size)

                elif args.bpeTokenizer:  # bpe tokenizer
                    if args.bpeChar:  # bpe char
                        if os.path.exists(vocab_json):
                            logger.info(f"Loading char BPE tokenizer")
                            from tokenizers import CharBPETokenizer
                            tokenizer = CharBPETokenizer(
                                os.path.join(tokenizer_name, "vocab.json"),
                                os.path.join(tokenizer_name, "merges.txt")
                            )

                            tokenizer.enable_truncation(max_length=block_size)

                    elif args.bpeSentence:  # bpe sentence
                        if os.path.exists(vocab_json):
                            logger.info(f"Loading sentence BPE tokenizer")
                            from tokenizers import SentencePieceBPETokenizer
                            tokenizer = SentencePieceBPETokenizer(
                                os.path.join(tokenizer_name, "vocab.json"),
                                os.path.join(tokenizer_name, "merges.txt")
                            )
                            tokenizer.enable_truncation(max_length=block_size)
                    else:  # bpe
                        logger.info("Loading BPE tokenizer")
                logger.info("Reading file %s", file_path)
                is_gzip = True if file_path[-3:] == '.gz' else False

                self.examples = []

                f = gzip.open(file_path, 'rt', encoding='utf8') if is_gzip else open(file_path, 'r', encoding='utf8')
                documents = f.read().split('\n\n')
                logger.info("Read {} documents".format(len(documents)))

                eos, speaker1, speaker2, speaker1_end, speaker2_end = tokenizer.convert_tokens_to_ids(SPECIAL_TOKENS_2)

                if args.useMecab:  # if tokenized input(use mecab tokenization before using our trained tokenizer)
                    mecab_tokenizer = MecabBasicTokenizer(do_lower_case=True,
                                                          mecab_dict_path=args.mecab_dict_path)
                tokenized_text = []
                combination= 4
                counter = 0
                for text in tqdm(documents):
                    for i, sentence in enumerate(text.split('\n')):
                        if len(sentence) > 1 and sentence != "\n":
                            speaker = speaker1 if i % 2 == 0 else speaker2
                            speaker_end = speaker1_end if i % 2 == 0 else speaker2_end
                            if args.useMecab:  # if tokenized input(use mecab tokenization before using our trained tokenizer)
                                tokenized_sentence = mecab_tokenizer.tokenize(sentence)
                                tokenized_sentence = tokenizer.encode(" ".join(tokenized_sentence))
                                ids = [speaker] + tokenized_sentence + [speaker_end]
                                tokenized_text.extend(ids)
                            else:
                                tokenized_sentence = tokenizer.encode(sentence)
                                tokenized_text.extend(tokenized_sentence)
                            counter += 1
                    if counter == combination:
                        tokenized_text_ = [eos] + tokenized_text + [eos]
                        if len(tokenized_text_) <= block_size:
                            self.examples.append(
                                tokenizer.build_inputs_with_special_tokens(tokenized_text_)
                            )
                        else:
                            for i in range(0, len(tokenized_text_) - block_size + 1, block_size):
                                self.examples.append(
                                    tokenizer.build_inputs_with_special_tokens(tokenized_text_[i: i + block_size])
                                )
                        counter = 0
                        tokenized_text = []

                if counter != 0:
                    tokenized_text_ = [eos] + tokenized_text + [eos]
                    if len(tokenized_text_) <= block_size:
                        self.examples.append(
                            tokenizer.build_inputs_with_special_tokens(tokenized_text_)
                        )
                    else:
                        for i in range(0, len(tokenized_text_) - block_size + 1, block_size):
                            self.examples.append(
                                tokenizer.build_inputs_with_special_tokens(tokenized_text_[i: i + block_size])
                            )
                    counter = 0

                torch.save(self.examples, cached_features_file, pickle_protocol=pickle.HIGHEST_PROTOCOL)
                logger.info(
                    f"Saving features into cached file "
                )

    def __len__(self):
        return len(self.examples)

    def __getitem__(self, i):
        return torch.tensor(self.examples[i], dtype=torch.long)




def padToMaxLength(len, pad_token, block_size):
    return [pad_token] * (block_size-len)


def get_dataset(model, tokenizer: PreTrainedTokenizer, model_args: ModelArguments, data_args: DataTrainingArguments,
                evaluate=False, ):
    file_path = data_args.eval_data_file if evaluate else data_args.train_data_file
    if data_args.line_by_line:
        return LineByLineTextDataset(tokenizer=tokenizer, args=model_args, file_path=file_path,
                                     block_size=data_args.block_size, overwrite_cache=data_args.overwrite_cache, model=model)
    elif data_args.personaTextDataset:
        return PersonaTextDataset(tokenizer=tokenizer, args=model_args, file_path=file_path, block_size=data_args.block_size,
            overwrite_cache=data_args.overwrite_cache, model=model)
    elif data_args.SQUADDataset:
        return SQUADDataset(tokenizer=tokenizer, args=model_args, file_path=file_path,
                                  block_size=data_args.block_size,
                                  overwrite_cache=data_args.overwrite_cache, model=model)
    else:
        return TextDataset(
            tokenizer=tokenizer, args=model_args, file_path=file_path, block_size=data_args.block_size,
            overwrite_cache=data_args.overwrite_cache, model=model
        )


def main():
    # See all possible arguments in src/transformers/training_args.py
    # or by passing the --help flag to this script.
    # We now keep distinct sets of args, for a cleaner separation of concerns.

    parser = HfArgumentParser((ModelArguments, DataTrainingArguments, TrainingArguments))
    model_args, data_args, training_args = parser.parse_args_into_dataclasses()

    if data_args.eval_data_file is None and training_args.do_eval:
        raise ValueError(
            "Cannot do evaluation without an evaluation data file. Either supply a file to --eval_data_file "
            "or remove the --do_eval argument."
        )

    if (
            os.path.exists(training_args.output_dir)
            and os.listdir(training_args.output_dir)
            and training_args.do_train
            and not training_args.overwrite_output_dir
    ):
        raise ValueError(
            f"Output directory ({training_args.output_dir}) already exists and is not empty. Use --overwrite_output_dir to overcome."
        )

    # Setup logging
    logging.basicConfig(
        format="%(asctime)s - %(levelname)s - %(name)s -   %(message)s",
        datefmt="%m/%d/%Y %H:%M:%S",
        level=logging.INFO if training_args.local_rank in [-1, 0] else logging.WARN,
    )
    logger.warning(
        "Process rank: %s, device: %s, n_gpu: %s, distributed training: %s, 16-bits training: %s",
        training_args.local_rank,
        training_args.device,
        training_args.n_gpu,
        bool(training_args.local_rank != -1),
        training_args.fp16,
    )
    logger.info("Training/evaluation parameters %s", training_args)

    # Set seed
    set_seed(training_args.seed)

    # Load pretrained model and tokenizer
    #
    # Distributed training:
    # The .from_pretrained methods guarantee that only one local process can concurrently
    # download model & vocab.

    if model_args.bertJapaneseTokenizer:
        _AutoTokenizer = BertJapaneseTokenizer  # We use BertJapaneseTokenizer
    elif model_args.bertTokenizer:  # we use bert tokenzier
        _AutoTokenizer = BertTokenizer
    else:
        _AutoTokenizer = AutoTokenizer

    if model_args.config_name:
        config = AutoConfig.from_pretrained(model_args.config_name, cache_dir=model_args.cache_dir)
    elif model_args.model_name_or_path:
        config = AutoConfig.from_pretrained(model_args.model_name_or_path, cache_dir=model_args.cache_dir)
    else:
        config = CONFIG_MAPPING[model_args.model_type]()
        logger.warning("You are instantiating a new config instance from scratch.")

    if model_args.bpeTokenizer:
        if model_args.tokenizer_name:
            tokenizer = _AutoTokenizer.from_pretrained(model_args.tokenizer_name, cache_dir=model_args.cache_dir, pad_to_max_length=True)
        elif model_args.model_name_or_path:
            tokenizer = _AutoTokenizer.from_pretrained(model_args.model_name_or_path, cache_dir=model_args.cache_dir, pad_to_max_length=True)
        else:
            raise ValueError(
                "You are instantiating a new {} tokenizer. This is not supported, but you can do it from another script, save it,"
                "and load it from here, using --tokenizer_name".format(_AutoTokenizer.__name__)
            )
    elif model_args.bertJapaneseTokenizer:
        if model_args.tokenizer_name:
            tokenizer = _AutoTokenizer.from_pretrained(model_args.tokenizer_name, word_tokenizer_type='mecab',
                                                       do_lower_case=False, cache_dir=model_args.cache_dir)
        elif model_args.model_name_or_path:
            tokenizer = _AutoTokenizer.from_pretrained(model_args.model_name_or_path, word_tokenizer_type='mecab',
                                                       do_lower_case=False, cache_dir=model_args.cache_dir)
        else:
            raise ValueError(
                "You are instantiating a new {} tokenizer. This is not supported, but you can do it from another script, save it,"
                "and load it from here, using --tokenizer_name".format(_AutoTokenizer.__name__)
            )
    elif model_args.bertTokenizer:
        if model_args.tokenizer_name:
            tokenizer = _AutoTokenizer.from_pretrained(model_args.tokenizer_name,
                                                       do_lower_case=False, cache_dir=model_args.cache_dir)
        elif model_args.model_name_or_path:
            tokenizer = _AutoTokenizer.from_pretrained(model_args.model_name_or_path,
                                                       do_lower_case=False, cache_dir=model_args.cache_dir)
        else:
            raise ValueError(
                "You are instantiating a new {} tokenizer. This is not supported, but you can do it from another script, save it,"
                "and load it from here, using --tokenizer_name".format(_AutoTokenizer.__name__)
            )
    else:
        if model_args.tokenizer_name:
            tokenizer = _AutoTokenizer.from_pretrained(model_args.tokenizer_name, cache_dir=model_args.cache_dir)
        elif model_args.model_name_or_path:
            tokenizer = _AutoTokenizer.from_pretrained(model_args.model_name_or_path, cache_dir=model_args.cache_dir)
        else:
            raise ValueError(
                "You are instantiating a new tokenizer from scratch. This is not supported, but you can do it from another script, save it,"
                "and load it from here, using --tokenizer_name"
            )

        # check mecab if arg(--useMecab) is true
    if model_args.useMecab and len(model_args.mecab_dict_path) == 0:
        raise ValueError(
            "You are using the argument --useMecab tokenizer without passing a mecab_dict_path."
        )
    if model_args.model_name_or_path:
        model = AutoModelWithLMHead.from_pretrained(
            model_args.model_name_or_path,
            from_tf=bool(".ckpt" in model_args.model_name_or_path),
            config=config,
            cache_dir=model_args.cache_dir,
        )
    else:
        logger.info("Training new model from scratch")
        model = AutoModelWithLMHead.from_config(config)

    model.resize_token_embeddings(len(tokenizer))

    if config.model_type in ["bert", "roberta", "distilbert", "camembert"] and not data_args.mlm:
        raise ValueError(
            "BERT and RoBERTa-like models do not have LM heads but masked LM heads. They must be run using the --mlm "
            "flag (masked language modeling)."
        )

    if data_args.block_size <= 0:
        data_args.block_size = tokenizer.max_len
        # Our input block size will be the max possible for the model
    else:
        data_args.block_size = min(data_args.block_size, tokenizer.max_len)

    # Get datasets
    train_dataset = get_dataset(tokenizer=tokenizer, model_args=model_args,
                                data_args=data_args, model=model) if training_args.do_train else None
    eval_dataset = get_dataset(tokenizer=tokenizer, model_args=model_args, data_args=data_args,
                               evaluate=True, model=model) if training_args.do_eval else None
    data_collator = DataCollatorForLanguageModeling(
        tokenizer=tokenizer, mlm=data_args.mlm, mlm_probability=data_args.mlm_probability
    )

    # Initialize our Trainer
    trainer = Trainer(
        model=model,
        args=training_args,
        data_collator=data_collator,
        train_dataset=train_dataset,
        eval_dataset=eval_dataset,
        prediction_loss_only=True,
    )

    # Training
    if training_args.do_train:
        model_path = (
            model_args.model_name_or_path
            if model_args.model_name_or_path is not None and os.path.isdir(model_args.model_name_or_path)
            else None
        )
        trainer.train(model_path=model_path)
        trainer.save_model()
        # For convenience, we also re-save the tokenizer to the same directory,
        # so that you can share your model easily on huggingface.co/models =)
        if trainer.is_world_master():
            tokenizer.save_pretrained(training_args.output_dir)

    # Evaluation
    if training_args.do_eval or model_args.eval_all_checkpoints:
        results = {}
        if model_args.eval_all_checkpoints:

            checkpoints = list(
                os.path.dirname(c) for c in
                sorted(glob.glob(training_args.output_dir + "/**/" + WEIGHTS_NAME, recursive=True))
            )
            checkpoints_folder = [x[0] for x in sorted(os.walk(training_args.output_dir))]
            checkpoints_folder.append(checkpoints_folder.pop(0))

            logging.getLogger("transformers.modeling_utils").setLevel(logging.WARN)  # Reduce logging
            logger.info("Evaluate the following checkpoints: %s", checkpoints)

            for i, checkpoint in enumerate(checkpoints):
                model = AutoModelWithLMHead.from_pretrained(
                    checkpoint,
                    from_tf=bool(".ckpt" in checkpoint),
                    config=config,
                    cache_dir=model_args.cache_dir,
                )
                trainer = Trainer(
                    model=model,
                    args=training_args,
                    data_collator=data_collator,
                    eval_dataset=eval_dataset,
                    prediction_loss_only=True,
                )

                logger.info("*** Evaluating  %s ***" % checkpoints_folder[i])
                eval_output = trainer.evaluate()

                perplexity = math.exp(eval_output["eval_loss"])
                result = {"perplexity": perplexity}

                output_eval_file = os.path.join(checkpoints_folder[i], "eval_results_lm.txt")

                if trainer.is_world_master():
                    with open(output_eval_file, "w") as writer:
                        logger.info("***** Eval results *****")
                        for key in sorted(result.keys()):
                            logger.info("  %s = %s", key, str(result[key]))
                            writer.write("%s = %s\n" % (key, str(result[key])))

        else:
            results = {}
            logger.info("*** Evaluate ***")

            eval_output = trainer.evaluate()

            perplexity = math.exp(eval_output["eval_loss"])
            result = {"perplexity": perplexity}

            output_eval_file = os.path.join(training_args.output_dir, "eval_results_lm.txt")
            if trainer.is_world_master():
                with open(output_eval_file, "w") as writer:
                    logger.info("***** Eval results *****")
                    for key in sorted(result.keys()):
                        logger.info("  %s = %s", key, str(result[key]))
                        writer.write("%s = %s\n" % (key, str(result[key])))

            results.update(result)
        return results


def _mp_fn(index):
    # For xla_spawn (TPUs)
    main()


if __name__ == "__main__":
    main()
