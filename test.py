from tqdm import tqdm

f= open('./data/train.txt', 'r', encoding='utf8')
documents = f.read().split('DIALOGUE_END')
examples = []
block_size = 1024
for text in tqdm(documents):
    tokenized_text = []
    for i, sentence in enumerate(text.split('\n')):
        if len(sentence) > 1 and sentence != "\n" and "THEME" not in sentence and 'DIALOGUE_END' not in sentence:
            speaker = 'speaker1' if "A" in sentence[:3] else 'speaker2'
            sentence = sentence[3:]
            tokenized_text.extend(sentence.split(" "))
    tokenized_text = tokenized_text * 1000
    print(len(tokenized_text))
    if len(tokenized_text) <= block_size:
        examples.append(tokenized_text)
    else:
        for i in range(0, len(tokenized_text) - block_size + 1, block_size):
            print("yeah")
            examples.append(tokenized_text[i: i + block_size])



